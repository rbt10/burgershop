<?php


namespace App\Entity;


class Boisson
{
    private $name;
    private $price;
    private $description;
    public static $boissons =[];

    /**
     * Boisson constructor.
     * @param $name
     * @param $price
     * @param $description
     */
    public function __construct($name, $price, $description)
    {
        $this->name = $name;
        $this->price = $price;
        $this->description = $description;
        self::$boissons [] =$this;
    }

    public static function createB (){

        $oasis = new Boisson("oasis",1.0, "Boisson sucrée multi-fruits");
        $coca = new Boisson("coca",1.2, "Boisson sucrée aux meilleurs gouts");
        $fanta = new Boisson("fanta",1.2, "Boisson sucrée à l'orange ");
        $fanta = new Boisson("mirinda",1.2, "Boisson sucrée à la fraise ");

    }

    /**
     * @return mixed
     */
    public function getname()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setname($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public static function getBoissons(): array
    {
        return self::$boissons;
    }

    /**
     * @param array $boissons
     */
    public static function setBoissons(array $boissons): void
    {
        self::$boissons = $boissons;
    }


    public static function getNameBoisson($name){
        foreach (self::$boissons as $boisson) {
            if (strtolower($boisson->name === $name)){
                return $boisson;
            }
        }
    }




}