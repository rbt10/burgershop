<?php

namespace App\Entity;
class Burger
{
    public $name;
    public $description;
    public $price;
    public static  $burger =[];

    public function __construct($name,$description,$price){
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
        self::$burger [] = $this;
    }

    public static function creerBurger(){
        $b1 = new Burger("BigMac","Salade, tomate, oignon, cornichon, steak", 8.90);
        $b2 = new Burger("tripleChease","Salade, tomate, oignon, cornichon, steak", 9.00);
        $b3 = new Burger("MacChicken","Salade, tomate, oignon, cornichon, chicken", 9.50);
    }

    public static function getName($name){
        foreach (self::$burger as $perso){
            if (strtolower($perso->name ) === $name){
                return $perso;
            }
        }
    }

}