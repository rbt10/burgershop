<?php

namespace App\Controller;

use App\Entity\Boisson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BoissonController extends AbstractController
{
    #[Route('/boisson', name: 'boisson')]
    public function index(): Response
    {
        Boisson::createB();
        return $this->render('boisson/index.html.twig', [
           'boissons' => Boisson::$boissons
        ]);
    }
    #[Route('/boisson/{name}', name: 'afficher_boisson')]
    public function afficherB($name): Response
    {
        Boisson::createB();
        $boisson = Boisson::getNameBoisson($name);
        return $this->render('boisson/afficherB.html.twig',[
            "boisson" => $boisson
        ]);
    }
}
