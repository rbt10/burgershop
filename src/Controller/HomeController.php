<?php

namespace App\Controller;

use App\Entity\Boisson;
use App\Entity\Burger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(): Response
    {
        return $this->render('home/index.html.twig');
    }

    #[Route('/burger', name: 'burger')]
    public function burgers(): Response
    {
        Burger::creerBurger();
        return $this->render('home/burger.html.twig',[
            "lesBurgers" => Burger::$burger
        ]);

    }
    #[Route('/burger/{name}', name: 'afficher_burger')]
    public function afficherBurger($name): Response
    {
        Burger::creerBurger();
        $burger = Burger::getName($name);
        return $this->render('home/perso.html.twig',[
        "burger" => $burger
    ]);

    }

}
